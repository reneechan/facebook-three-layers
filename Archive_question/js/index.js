﻿var preloadedInterstitial = null;
var preloadedRewardedVideo = null;
var platform = "";//平台
var firstopen = true;//首次进入
var Global = Global || {};
Global.isbool = true;
Global.gametime = 0;
var nScrollHight; //滚动距离总长(注意不是滚动条的长度)
var nScrollTop;   //滚动到的当前位置
var nDivHight;
var pagenum = 0;//当前页数-1
var tmtpye;
var wtitle;
var thisid;
var thisqid;
var iflast;
var fmimg;
var messfmimg = "";
var messqid;
var messid;
var ifImgAnswer;
var toansnow = false;
var rebootshare = false;
var searchtype;//搜索id
var firsttime = 0;
var sharer = "";
var vipid;
// 2019-9-26 09:48:47 添加是否有选项
var ifHaveQuestion = false; // 是否有选项
var zn = {
    "hot": "最火",
    "week": "本周",
    "new": "最新",
    "interest": "趣味",
    "share": "分享到FB",
    "again": "再測一次",
    "type": "測試題：",
    "lang": "选择语言",
    "sharefm": "分享封面",
    "yes": "太準了！",
    "no": "不太準！",
    "ans1": "答題完畢,是否查看結果？",
    "ans2": "查看結果",
    "ans3": "取消",
    "gamename": "遊戲名稱:",
    "gameid": "玩家ID:",
    "tmsearch": "ID搜索:",
    "keyname": "請輸入ID...",
    "search": "搜索",
    "close": "關閉",
    "copyyes": "復制成功",
    "copyfail": "復制失败",
    "notype": "暂无此分类",
    "morelike": "你應該也喜歡…",
    "error": "獲取出錯",
    "fenxi": "結果分析中...",
    "moregame": "更多遊戲：",
    "fuzhi": "復制",
    "cchy": "測測好友"
};
var jp = {
    "hot": "人気",
    "week": "今週",
    "new": "新着",
    "interest": "楽しい",
    "share": "FBでシェア",
    "again": "もう一回",
    "type": "テスト:",
    "lang": "言語を選択",
    "sharefm": "カバーでシェア",
    "yes": "当たる",
    "no": "外れる",
    "ans1": "終わりました。結果を見ますか？",
    "ans2": "見る",
    "ans3": "見ない",
    "gamename": "ゲーム名前:",
    "gameid": "プレーヤーID:",
    "tmsearch": "タイトル検索:",
    "keyname": "キーワードを入力...",
    "search": "検索",
    "close": "閉じる",
    "copyyes": "コピー成功",
    "copyfail": "コピー失敗",
    "notype": "この分類はなし",
    "morelike": "好きかも",
    "error": "エラーを取得",
    "fenxi": "結果分析...",
    "moregame": "その他：",
    "fuzhi": "コピー",
    "cchy": "友達をテストする"
};
var en = {
    "hot": "HOT",
    "new": "NEW",
    "week": "WEEK",
    "share": "Share to FB",
    "interest": "FUN",
    "again": "Once again",
    "type": "Test:",
    "lang": "Language",
    "sharefm": "Share cover",
    "yes": "Good",
    "no": "Bad",
    "ans1": "Check the results?",
    "ans2": "Ok",
    "ans3": "Cancel",
    "gamename": "Game Name:",
    "gameid": "Player ID:",
    "tmsearch": "Topic Search:",
    "keyname": "Please enter the keyword...",
    "search": "Search",
    "close": "Close",
    "copyyes": "Successful replication",
    "copyfail": "Replication failure",
    "notype": "There is nothing for the time being",
    "morelike": "You should also like it…",
    "error": "Get error",
    "fenxi": "Loading...",
    "moregame": "more:",
    "fuzhi": "copy",
    "cchy": "Invite friends to test"
};
var pt = {
    "hot": "Quente",
    "week": "Semana",
    "new": "Novo",
    "share": "Compartilhar para FB",
    "again": "Novamente",
    "type": "Perguntas do teste:",
    "lang": "Idioma",
    "sharefm": "Compartilhar capa",
    "interest": "Interesse",
    "yes": "Sim",
    "no": "Cancelado",
    "ans1": "Vê o resultado ou não?",
    "ans2": "Veja",
    "ans3": "Não ver",
    "gamename": "nome do jogo:",
    "gameid": "ID do jogador:",
    "tmsearch": "pesquisa de títulos:",
    "keyname": "palavra-chave...",
    "search": "pesquisar",
    "close": "fechar",
    "copyyes": "copy success",
    "copyfail": "cópia falhou",
    "notype": "não existe",
    "morelike": "Também deve gostar ...",
    "error": "Obter erro",
    "fenxi": "Analisado...",
    "moregame": "Mais:",
    "fuzhi": "Copiar",
    "cchy": "Deixa amigos testar"
};
var fr = {
    "hot": "le plus chaud",
    "week": "la semaine",
    "new": "le plus récent",
    "interest": "l'internaute",
    "share": "partager avec FB",
    "again": "encore une fois",
    "type": "la question du test：",
    "lang": "sélectionner une langue",
    "sharefm": "partager l'image",
    "yes": "oui",
    "no": "non",
    "ans1": "fin de la réponse , vérifiez-vous le résultat ?",
    "ans2": "examiner le résultat",
    "ans3": "annuler",
    "gamename": "le nom du jeu:",
    "gameid": "le ID du joueur:",
    "tmsearch": "chercher le ID:",
    "keyname": "entrer le ID s'il vous plaît",
    "search": "chercher",
    "close": "fermer",
    "copyyes": "le succès de la copie",
    "copyfail": "l'échec de la copie",
    "notype": "il n'y a pas de cette classification",
    "morelike": "même type…",
    "error": "erreur",
    "fenxi": "l'analyse le résultat...",
    "moregame": "Plus:",
    "fuzhi": "Copia",
    "cchy": "Invitez vos amis à tester"
};
var de = {
    "hot": "Heiß",
    "week": "Woche",
    "new": "Neu",
    "interest": "Interesse",
    "share": "teilen FB",
    "again": "noch Mal",
    "type": " Test:",
    "lang": "Sprache wählen",
    "sharefm": "Deckung teilen",
    "yes": "ja!",
    "no": "nein!",
    "ans1": "die Ergebnisse überprüft ，ja oder nein ?",
    "ans2": "Ergebnisse anzeigen",
    "ans3": "ausfallen",
    "gamename": "Spielname:",
    "gameid": "Spieler ID:",
    "tmsearch": "ID-Suche:",
    "keyname": "ID eingeben...",
    "search": "suchen",
    "close": "zumachen",
    "copyyes": "Kopieren erfolgreich",
    "copyfail": "Kopieren fehlgeschlagen",
    "notype": "Keine  Kategorie",
    "morelike": "Du solltest mögen...",
    "error": "Lesefehler",
    "fenxi": "Ergebnisanalyse...",
    "moregame": "Mehl:",
    "fuzhi": "Kopieren",
    "cchy": "Freunde testen"
};
var th = {
    "hot": "สุดฮ็อต",
    "week": "สัปดาห์นี้",
    "new": "ล่าสุด",
    "interest": "สนุก",
    "share": "แชร์ไปที่ FB",
    "again": "ทดสอบอีกครั้ง",
    "type": " ทดสอบคำถาม:",
    "lang": "เลือกภาษา",
    "sharefm": "แชร์ฝาครอบ",
    "yes": "ถูกต้อง!",
    "no": "ไม่ถูกต้อง!",
    "ans1": "คำตอบเป็นที่เรียบร้อยแล้ว，ดูผลลัพธ์ ?",
    "ans2": "ดูผลลัพธ์",
    "ans3": "ยกเลิก",
    "gamename": "ชื่อเกม:",
    "gameid": "ID ผู้เล่น:",
    "tmsearch": "รหัสการค้นหา:",
    "keyname": "กรุณาใส่ ID...",
    "search": "ค้นหา",
    "close": "ปิด",
    "copyyes": "คัดลอกสำเร็จ",
    "copyfail": "การคัดลอกล้มเหลว",
    "notype": "ไม่มีหมวดหมู่ดังกล่าว",
    "morelike": "คุณควรจะชอบ...",
    "error": "อ่านข้อผิดพลาด",
    "fenxi": "กำลังวิเคราะห์ผลลัพธ์...",
    "moregame": "เกมมากขึ้น:",
    "fuzhi": "ลอก",
    "cchy": "ทดสอบเพื่อน"
}

$(function () {
    setSize();
    FBInstant.initializeAsync().then(function () {
        FBInstant.startGameAsync().then(onStart);
    });
    $(window).resize(function () {
        setSize();
        $("html").css("font-size", $("body").width());
    });
    $("#app .navbar ul li").click(function () {
        if ($(this).hasClass("now") != true) {
            var index = $(this).index();
            Global.showpage = index;
            $("#app .navbar ul li").removeClass("now");
            $(this).addClass("now");
            getinfo(index);
        }
    });
    $("#app .listani").delegate(".mlayoutlist", "click", function () {
        var thisid = $(this).attr("value");
        toquestion(thisid);
    });
    $("#app .listani").delegate(".resultbtns button:eq(1)", "click", function () {
        showRewardedVideo(1);
        //toquestion(thisqid);
    });
    $("#app .listani").delegate(".zan .zanlist", "click", function () {
        var index = $(this).index();
        if (index == 0) {
            $(this).find("img").attr("src", "images/yed.png");
            $(this).find("p").addClass("now");
            dzsc(1, thisqid);
        } else {
            $(this).find("img").attr("src", "images/ned.png")
            $(this).find("p").addClass("now");
            dzsc(0, thisqid);
        }
        $("#app .listani .zan .zanlist").css("pointer-events", "none");
    });
    $(".header .btn-menu img").click(function () {
        $(".sliderleft").toggle();
    });
    $(".header .logo img").click(function () {
        $.vip();
    });
    $(".sliderleft ul li").click(function () {
        Global.locale = $(this).attr("value");
        translation(1);
        $(".sliderleft").attr("style", "display: none;");
    });
    $.question = function (obj) {
        if (obj) {
            var addcss = "";
            var info = "";
            var question_html = "";
            $.ajax({
                type: "GET",
                // url: "https://api.2loveyou.com:8443/data/center/basic/TestQuestions?sort=openCount&dir=DESC&size=10&page=0" + "&type=" + obj.tjtype + "&language=" + Global.lang,//+ "&ifImgAnswer=1"
                // url: "https://api.2loveyou.com:8443/data/center/basic/TestQuestions?sort=openCount&dir=DESC&size=10&page=0" + "&type=" + obj.tjtype + "&language=" + Global.lang,//+ "&ifImgAnswer=1"
                url: "http://kquiz.co/api.php",    
                data: {},
                dataType: 'json',
                success: function (data) {
                    var zsimgurl = "";
                    for (var i = 0; i <= data.data.list.length; i++) {

                        // 2019-9-26 11:11:45 添加

                        if (i != data.data.list.length) {
                            if (i == 0 && data.data.list[i].questionId == obj.pid) {
                                addcss = "hide";
                            } else {
                                addcss = "";
                            }
                            if (data.data.list[i].hCoverUrl2 != null) {
                                zsimgurl = data.data.list[i].hCoverUrl2 + "?x-oss-process=image/resize,w_600";
                            } else {
                                zsimgurl = data.data.list[i].coverUrl2 + "?x-oss-process=image/resize,w_600";
                            }
                            info += '<div class="mlayoutlist ' + addcss + '" value="' + data.data.list[i].questionId + '">' +
                                '<div class="pict" >' + '<span><img src="images/hot.png"></span>' +
                                '<img src="' + zsimgurl + '" /></div >' +
                                '<h5>' + data.data.list[i].title + '</h5>' +
                                '</div>';
                        } else {

                            console.log("platform:" + Global.platform);

                            // 创建推广HTML
                            function createTGHTML() {
                                var res = "";
                                if (Global.platform != "IOS") {
                                    res = '<div class="winb3 clearfix">\n' +
                                        '                <div class="winplay"><em>' + get_lan("moregame") + '</em></div>\n' +
                                        '                <div class="txt wingamelogo" style="padding-right:0.03rem; "><img src="images/qmcg.png" onclick="togame(this)" id="853784188326610"></div>\n' +
                                        '                <div class="txt wingamelogo" ><img src="images/tyt.png" onclick="togame(this)" id="883029935407307"></div>\n' +
                                        '                <div class="txt wingamelogo"><img src="images/qmcm.png" onclick="togame(this)" id="465692113990670"></div>\n' +
                                        '                <div class="txt wingamelogo"><img src="images/xcc.png" onclick="togame(this)" id="1515244195266806"></div>\n' +
                                        '\n' +
                                        '            </div>';
                                }
                                return res;
                            }

                            question_html =
                                // 添加推广
                                '<div id="mainlayout">' +
                                obj.div +

                                createTGHTML() +

                                '<div class="recommend">' +
                                '<h3>' + get_lan("morelike") + '</h3>' +
                                info +
                                '</div>' +
                                '</div>';
                            if ($("#mainlayout").length <= 0) {
                                $(".listani").append(question_html);
                                $("#loading").addClass("hide");
                                $("#mainlayout").css("height", $(window).outerHeight() - $(".page-header").height() - 20);
                                //$(".listani").css("height", $(".answerbox").outerHeight());
                                $("#fx").removeClass("hide");
                                //$(".listani").css("height", "1.2rem");
                                scroll(1);
                            }
                        }
                    }
                }, error: function (reponse) {

                }
            });
        }
    };
    $.shareload = function () {
        var shareload_html = '<div class="aboxload" id="shareload">' +
            '<div id="loading1">' +
            '<div id="loading-center">' +
            '<div id="loading-center-absolute">' +
            '<div class="object1" id="object_one"></div>' +
            '<div class="object1" id="object_two"></div>' +
            '<div class="object1" id="object_three"></div>' +
            '</div>' +
            '</div>' +
            '</div >' +
            '</div >';
        if ($("#shareload").length <= 0) {
            $("body").prepend(shareload_html);
        } else {
            $("#shareload").remove();
        }
    }
    $.vip = function () {
        var vip_html = '<div class="gameinfobg" id="vip">' +
            '<div class="gameinfobox">' +
            '<table>' +
            '<tr>' +
            '<th><label>' + get_lan("gamename") + '</label></th>' +
            '<td>WHH</td>' +
            '</tr>' +
            '<tr>' +
            '<th><label>' + get_lan("gameid") + '</label></th>' +
            '<td><button data-clipboard-text="' + Global.playerid + '|487028831860750" data-clipboard-action="copy" id="copy">' + get_lan("fuzhi") + '</button></td>' +
            '</tr>' +
            '<tr>' +
            '<th><label>' + get_lan("tmsearch") + '</label></th>' +
            '<td><input type="text" placeholder="' + get_lan("keyname") + '" id="typeinput"/></td>' +
            '</tr>' +
            '</table>' +
            '<div class="gameinfobtn clearfix">' +
            '<button id="search">' + get_lan("search") + '</button>' +
            '<button id="close">' + get_lan("close") + '</button>' +
            '</div>' +
            '</div>' +
            '</div >';
        if ($("#vip").length <= 0) {
            $("body").append(vip_html);
            var clipboard = new Clipboard('#copy');
            clipboard.on('success', function (e) {
                tantxt(get_lan("copyyes"));
            });
            clipboard.on('error', function (e) {
                tantxt(get_lan("copyfail"));
            });
        }
        $("#search").click(function () {
            searchtype = parseInt($("#typeinput").val());
            toquestionSearch(searchtype);
            //getinfo(4);
            $("#vip").remove();
            $("#search").unbind();
        });
        $("#close").click(function () {
            $("#vip").remove();
            $("#close").unbind();
        });
    }
    $.xunwen = function (obj) {
        if (obj) {
            var xunwen_html = '<div class="askbg" id="xunwen">' +
                '<div class="askbox">' +
                '<p>' + get_lan("ans1") + '</p>' +
                '<div class="askbtn clearfix">' +
                '<button id="seeanswer"><img src="images/videoicon.png" />' + get_lan("ans2") + '</button>' +
                // '<button id="seeanswer"><img src="'+img+'" />' + get_lan("ans2") + '</button>' +
                '<button id="notsee">' + get_lan("ans3") + '</button>' +
                '</div>' +
                '</div >' +
                '</div>';
        }
        if ($("#xunwen").length <= 0) {
            //if (Global.lang == "7") {
            //    toanswer(obj.one, obj.two, obj.three, obj.four);
            //}
            //else {
            //    $("body").append(xunwen_html);
            //}
            $("body").append(xunwen_html);
        }
        $("#seeanswer").click(function () {
            // display the alert box for showing resutl
            $("#xunwen").remove();
            var logged = FBInstant.logEvent(
                'Adclick',
                1,
            );
            console.log(logged);
            showRewardedVideo(0)
            console.log("here!");
            console.log(obj);
            toanswer(obj.one, obj.two, obj.three, obj.four);
        });
        $("#notsee").click(function () {
            $("#mainlayout .answerbox button").removeClass("unclick");
            $("#xunwen").remove();
            $("#notsee").unbind();
        });
    }
});

function setSize() {
    var ww = $(window).outerWidth();
    var wh = $(window).outerHeight();
    if ((ww / wh) > (9 / 16)) {
        var bw = parseInt($(window).outerHeight() * 9 / 16);
        $("body").css("width", bw);
        $("html").css("font-size", parseInt($(window).outerHeight() * 9 / 16));
    } else {
        $("body").css("width", ww);
        $("html").css("font-size", ww);
    }
    $(".listani").css("height", $(window).outerHeight() - $(".page-header").height());
}

function tantxt(t) {
    var nn = parseInt(Math.random() * 200 + 1);
    $("body").append("<div class=\"popwindow nn" + nn + "\" style=\"line-height:" + $("body").outerHeight() / 2 + "px\"><div class=\"popwindowbox\" style=\"text-align:center\"><div class=\"tantxt\">" + t + "</div></div></div>")
    setTimeout(function () {
        $("body .popwindow.nn" + nn).remove();
    }, 2000)
}

function onStart() {
    Global.myphoto = FBInstant.player.getPhoto();//头像
    Global.locale = FBInstant.getLocale();//语言
    Global.playerid = FBInstant.player.getID();//id
    Global.playername = FBInstant.player.getName();//名字
    Global.platform = FBInstant.getPlatform();//平台
    if (Global.platform == "IOS") {

    } else {

    }
    translation(0);
    var supportedAPIs = FBInstant.getSupportedAPIs();
    if (supportedAPIs.includes('getInterstitialAdAsync') && supportedAPIs.includes('getRewardedVideoAsync')) {
        //addload();
        vedioload();
    }
    preLoadIcon();
}

// 预加载看视频小图标 videoicon
function preLoadIcon() {
    var img = new Image();
    img.src = "images/videoicon.png";
    if (img.complete) {
        console.log("preload okd");
        return;
    }
    img.onload = function () {
        console.log("preload ok");
    };
    var img1 = new Image();
    img1.src = "images/qmcg.png";
    var img2 = new Image();
    img2.src = "images/tyt.png";
    var img3 = new Image();
    img3.src = "images/qmcm.png";
    var img4 = new Image();
    img4.src = "images/xcc.png";
}

// 交叉推广
function togame(t) {
    // 465692113990670 猜谜
    // 853784188326610 音乐
    // 883029935407307 跳跳
    // 1515244195266806 秀才
    var id = $(t).attr("id");
    FBInstant.switchGameAsync(id).catch(function (e) {
        // Handle game change failure
        console.log("switchGameAsync:", e);
    });
}

function main() {
    Global.showpage = 0;
    if (FBInstant.getEntryPointData() != null) {
        if (FBInstant.getEntryPointData().shareqid != null) {
            var toid = FBInstant.getEntryPointData().shareqid;
            toquestion(toid);
            if (FBInstant.getEntryPointData().shareuid != null) {
                var authorid = "";
                var hyuid = FBInstant.getEntryPointData().shareuid;
                if (FBInstant.getEntryPointData().authorid != null) {
                    authorid = FBInstant.getEntryPointData().authorid;
                } else {
                    authorid = "0";
                }
                var hyck = hyuid + "_" + toid;
                sharer = hyck;
                var s = document.cookie;
                if (s.indexOf(hyck + '=1') == -1) {
                    var address = "zh";
                    if (null != Global.locale && "" != Global.locale) {
                        address = Global.locale;
                    }

                    console.log("locate:" + Global.locale);
                    $.ajax({
                        type: "GET",
                        url: "http://kquiz.co/api_contentDetail.php?qId=" + toid + "&source=1",
                        // url: "https://api.2loveyou.com:8443/data/center/basic/testQuestions/contentDetail?qId=" + toid + "&source=1",
                        crossDomain: true,
                        dataType: 'json',
                        data: {},
                        type: "json",
                        success: function (data) {
                            var questionlang = data.data[0].lang;
                            if (questionlang == 0) {
                                $.ajax({
                                    type: "POST",
                                    url: "https://stat.fbquickgames.com/stat/in?fid=" + hyuid + "&gid=487028831860750" + "&qid=" + toid + "&userid=" + authorid,
                                    data: {},
                                    success: function (data) {
                                        var d = new Date();
                                        d.setHours(d.getHours() + 24); //有效期24小时
                                        document.cookie = hyck + '=1;expires=' + d.toGMTString();//设置cookie
                                        console.log("hydata is save");
                                    }, error: function (reponse) {

                                    }
                                });
                            }
                            if (address.indexOf("zh") < 0) {

                                if (questionlang == 7) {
                                    $.ajax({
                                        type: "POST",
                                        url: "https://brword.borderlessbd.com/stat/newin?fid=" + hyuid + "&gid=487028831860750&qid=" + toid + "&userid=" + authorid,
                                        data: {},
                                        success: function (data) {
                                            console.log("hydata brword is save");
                                        }, error: function (reponse) {

                                        }
                                    });
                                }
                                else if (questionlang == 1) {
                                    $.ajax({
                                        type: "POST",
                                        url: "https://zaword.borderlessbd.com/stat/newin?fid=" + hyuid + "&gid=487028831860750&qid=" + toid + "&userid=" + authorid,
                                        data: {},
                                        success: function (data) {
                                            console.log("hydata zaword is save");
                                        }, error: function (reponse) {

                                        }
                                    });
                                }
                                else if(questionlang == 6){ 
                                    $.ajax({
                                        type: "POST",
                                        url: "https://jpword.borderlessbd.com/stat/newin?fid=" + hyuid + "&gid=487028831860750&qid=" + toid + "&userid=" + authorid,
                                        data: {},
                                        success: function (data) {
                                            console.log("hydata jpword is save");
                                        }, error: function (reponse) {

                                        }
                                    });
                                }
                                else if(questionlang == 8){ 
                                    $.ajax({
                                        type: "POST",
                                        url: "https://frword.borderlessbd.com/stat/newin?fid=" + hyuid + "&gid=487028831860750&qid=" + toid + "&userid=" + authorid,
                                        data: {},
                                        success: function (data) {
                                            console.log("hydata frword is save");
                                        }, error: function (reponse) {

                                        }
                                    });
                                }

                                // if (data.data[0].lang != 0) {
                                //     $.ajax({
                                //         type: "POST",
                                //         url: "https://word.borderlessbd.com/stat/newin?fid=" + hyuid + "&gid=487028831860750&qid=" + toid + "&userid=" + authorid,
                                //         data: {},
                                //         success: function (data) {
                                //             var d = new Date();
                                //             d.setHours(d.getHours() + 24); //有效期24小时
                                //             document.cookie = hyck + '=1;expires=' + d.toGMTString();//设置cookie
                                //             console.log("hydata new is save");
   

                                //         }, error: function (reponse) {
                                //         }
                                //     });
                                // }
                            }


                        }, error: function (data) {

                        }
                    });


                }
            }
            if (FBInstant.getEntryPointData().auto != null) {
                var uid_tid = FBInstant.getEntryPointData().auto;
                var qid = "questionId=" + uid_tid.split("_")[1];
                var fid = "facebookId=" + uid_tid.split("_")[0];
                $.ajax({
                    type: "GET",
                    url: "https://api.2loveyou.com:8443/data/center/basic/testQuestions/updateShareOpenCount?" + qid + "&" + fid,
                    data: {},
                    success: function (reponse) {
                        console.log("autoopen+1");
                    }, error: function (reponse) {

                    }
                });
            }
        }
        else { 
            getinfo(0);
        }
    } else {
        getinfo(0);
    }
}

function addload() {
    FBInstant.getInterstitialAdAsync('487028831860750_528864561010510').then(function (interstitial) {
        preloadedInterstitial = interstitial;
        return preloadedInterstitial.loadAsync();
    }).catch(function (err) {
        console.log(err);
    });
}

function vedioload() {
    console.log("vedioload");
    FBInstant.getRewardedVideoAsync('487028831860750_500633133833653').then(function (rewarded) {
        preloadedRewardedVideo = rewarded;
        return preloadedRewardedVideo.loadAsync();
    }).catch(function (err) {
        console.log(err);
    });
}

function showInterstitial() {
    preloadedInterstitial.showAsync()
        .then(function () {
            addload();
            firsttime = new Date().getTime();
        })
        .catch(function (e) {
            console.log(e.message);
        });
}

function showRewardedVideo(t) {
    if (preloadedRewardedVideo == null) {
        console.log("preloadedRewardedVideo is null");
        return;
    }
    preloadedRewardedVideo.showAsync()
        .then(function () {
            vedioload();
            if (t == 0) {
                var logged = FBInstant.logEvent(
                    'Adsee',
                    1,
                );
                console.log(logged);
                firsttime = new Date().getTime();
            } else if (t == 1) {
                toquestion(thisqid);
            }
        }).catch(function (e) {
            console.log(e);
            if (t == 1) {
                toquestion(thisqid);
            }
        });
}

function getinfo(t) {
    var type = "";
    var itmtype = "";
    //if (Global.lang != 7 && Global.lang != 6 && Global.lang != 1 && Global.lang != 8) {
    //    itmtype = "quwei";
    //}
    if (Global.lang == 0) {
        itmtype = "quwei";
    }
    if (t == 0) {
        type = "sort=openCount&dir=DESC&size=10&page=0&type=" + itmtype;
    } else if (t == 1) {
        type = "sort=openCount7&dir=DESC&size=10&page=0&type=" + itmtype;
    } else if (t == 2) {
        type = "sort=createDate&dir=DESC&size=10&page=0&type=" + itmtype;
    } else if (t == 3) {
        type = "sort=openCount&dir=DESC&size=10&page=0&type=" + itmtype;
    } else if (t == 4) {
        type = "title=" + searchtype + "&size=10&page=0";
    }
    var info = "";
    var addcss = "";
    $.ajax({
        type: "GET",
        crossDomain: true,
        // url: "https://api.2loveyou.com:8443/data/center/basic/TestQuestions?" + type + "&language=" + Global.lang,//+ "&ifImgAnswer=1"
        // url: "https://api.2loveyou.com:8443/data/center/basic/TestQuestions?ifImgAnswer=2&qStatus=-1&page=0&size=10",
        url: "http://kquiz.co/api.php",        
        data: {
        },
        dataType: 'json',
        success: function (data) {
            console.log(data);
            if (data.data.list.length != 0) {
                var zsimgurl = "";
                Global.quenum = data.data.total;
                for (var i = 0; i <= data.data.list.length; i++) {
                    if (i != data.data.list.length) {
                        //if (i == 0) {
                        //    addcss = "nowbg";
                        //}
                        //else {
                        //    addcss = "";
                        //}
                        if (data.data.list[i].hCoverUrl2 != null) {
                            zsimgurl = data.data.list[i].hCoverUrl2 + "?x-oss-process=image/resize,w_600";
                        } else {
                            zsimgurl = data.data.list[i].coverUrl2 + "?x-oss-process=image/resize,w_600";
                        }
                        var hotPngHtml = "";
                        if (type.indexOf("openCount") > -1) {
                            hotPngHtml = "<span><img src=\"images/hot.png\"></span>";
                        }
                        info += '<div class="mlayoutlist ' + addcss + '" value="' + data.data.list[i].questionId + '">' +
                            '<div class="pict" >' + hotPngHtml + '<img src="' + zsimgurl + '" /></div >' +
                            '<h5>' + data.data.list[i].title + '</h5>' +
                            '</div>';
                    } else {
                        if ($("#mainlayout").length > 0) {
                            $("#mainlayout").unbind().remove();
                        }
                        $("#loading").addClass("hide");
                        $("#app .listani").append('<div id="mainlayout">' + info + '</div>');
                        $("#mainlayout").css("height", $(window).outerHeight() - $(".page-header").height() - 20);
                        scroll(0);
                        if (t == 4) {
                            Global.showpage = 4;
                            $("#app .navbar ul li").removeClass("now");
                        }
                    }
                }
            } else {
                tantxt(get_lan("notype"));
            }
        }, error: function (reponse) {

        }
    });
}

function addinfo() {
    var cstype = "";
    var itmtype = "";
    var newpage = Math.ceil(($(".mlayoutlist").length) / 10);
    if (newpage > pagenum) {
        pagenum = newpage;
        if (Global.lang == 0) {
            itmtype = "quwei";
        }

        //if (Global.lang != 7) {
        //    itmtype = "quwei";
        //}
        if (Global.showpage == 0) {
            cstype = "sort=openCount&dir=DESC&type=" + itmtype;
        } else if (Global.showpage == 1) {
            cstype = "sort=openCount7&dir=DESC&type=" + itmtype;
        } else if (Global.showpage == 2) {
            cstype = "sort=createDate&dir=DESC&type=" + itmtype;
        } else if (Global.showpage == 3) {
            cstype = "sort=openCount&dir=DESC&type=" + itmtype;
        } else if (Global.showpage == 4) {
            cstype = "title=" + searchtype;
        }
        $.ajax({
            type: "GET",
            url: "https://api.2loveyou.com:8443/data/center/basic/TestQuestions?" + cstype + "&size=10&page=" + newpage + "&language=" + Global.lang,
            data: {},
            success: function (data) {
                var info = '';
                var zsimgurl = "";
                for (var i = 0; i <= data.data.list.length; i++) {
                    if (i != data.data.list.length) {
                        if (data.data.list[i].hCoverUrl2 != null) {
                            zsimgurl = data.data.list[i].hCoverUrl2 + "?x-oss-process=image/resize,w_600";
                        } else {
                            zsimgurl = data.data.list[i].coverUrl2 + "?x-oss-process=image/resize,w_600";
                        }
                        info += '<div class="mlayoutlist" value="' + data.data.list[i].questionId + '">' +
                            '<div class="pict" > <img src="' + zsimgurl + '" /></div >' +
                            '<h5>' + data.data.list[i].title + '</h5>' +
                            '</div>';
                    } else {
                        $("#mainlayout").append(info);
                        Global.isbool = true;
                    }

                }
            }, error: function (reponse) {

            }
        });
    }
}

function addtjinfo() {
    var addcss = "";
    var info = "";
    var newpage = Math.ceil(($(".mlayoutlist").length) / 10);
    if (newpage > pagenum) {
        pagenum = newpage;
        $.ajax({
            type: "GET",
            url: "https://api.2loveyou.com:8443/data/center/basic/TestQuestions?sort=openCount&dir=DESC&size=10&page=" + newpage + "&type=" + tmtpye + "&language=" + Global.lang,//+ "&ifImgAnswer=1"
            data: {},
            success: function (data) {
                var zsimgurl = "";
                for (var i = 0; i <= data.data.list.length; i++) {
                    if (i != data.data.list.length) {
                        //if (i == 0 && data.data.list[i].questionId == thisqid) {
                        //    addcss = "hide";
                        //}
                        //else {
                        //    addcss = "";
                        //}
                        if (data.data.list[i].hCoverUrl2 != null) {
                            zsimgurl = data.data.list[i].hCoverUrl2 + "?x-oss-process=image/resize,w_600";
                        } else {
                            zsimgurl = data.data.list[i].coverUrl2 + "?x-oss-process=image/resize,w_600";
                        }
                        info += '<div class="mlayoutlist ' + addcss + '" value="' + data.data.list[i].questionId + '">' +
                            '<div class="pict" > <img src="' + zsimgurl + '" /></div >' +
                            '<h5>' + data.data.list[i].title + '</h5>' +
                            '</div>';
                    } else {
                        $("#mainlayout .recommend").append(info);
                        Global.isbool = true;
                    }
                }
            }, error: function (reponse) {

            }
        });
    }

}

function toquestion(t) {
    console.log("toquestion");
    var pid = t;
    thisqid = pid;
    wtitle = "";
    var redio = "";
    var img = "";
    var qbutton = "";
    var tmjson;
    var btntitle = "";
    var zsimgurl = "";
    var tjtype = "";
    var infodiv = "";
    var imgdiv = "";
    var miaoshu = "";
    $.ajax({
        type: "GET",
        // contentDetail
        url: "https://kquiz.co/api_contentDetail.php?qId=" + pid + "&source=1",
        // url: "https://api.2loveyou.com:8443/data/center/basic/testQuestions/contentDetail?qId=" + pid + "&source=1",
        crossDomain: true,
        dataType: 'json',
        data: {
            
        },
        success: function (data) {
            if (data.data.length == 0) {
                tantxt(get_lan("error"));
            } else {
                // updateopen(pid);
                data.data[0].vipId == null ? vipid = "0" : vipid = data.data[0].vipId;
                thisid = data.data[0].id;
                // tmjson = JSON.parse(data.data[0].json3);
                tmjson = data.data[0].json3;
                fmimg = data.data[0].cover_url3;
                tmtpye = tjtype = data.data[0].type;
                ifImgAnswer = data.data[0].if_img_answer;

                // 判断是否有选项
                if (data.data[0].if_last != null) {
                    ifHaveQuestion = data.data[0].if_last != 2;
                } else {
                    ifHaveQuestion = true;
                }
                // 判断是否有选项

                $("#app .navbar ul li").removeClass("now");
                if (rebootshare == false) {
                    if (data.data[0].if_last != "2") {
                        toansnow = false;
                        iflast = parseInt((tmjson.length - 1));
                        if (data.data[0].type == "quwei") {
                            if (Global.lang == "0") {
                                wtitle = data.data[0].title + " " + tmjson[iflast].question;
                            } else {
                                wtitle = data.data[0].title;
                            }
                        } else {
                            wtitle = tmjson[iflast].question;
                        }
                        if (tmjson[iflast].audio != null) {
                            if (tmjson[iflast].audio.toString() != "") {
                                redio = "<audio controls='controls' style='width:100%;' src='" + tmjson[x].audio.toString() + "'></audio>";
                            } else {
                                redio = "";
                            }
                        }
                        if (tmjson[iflast].img.toString() != "") {
                            zsimgurl = tmjson[iflast].img + "?x-oss-process=image/resize,w_600";
                        }
                        for (var key in tmjson[iflast].answer) {
                            var item = tmjson[iflast].answer[key];
                            if (item.title != null) {
                                if (item.title.indexOf("img") > 0) {
                                    btntitle = item.title.replace("class='img-responsive'", "class='img' style='border-radius: 10px'");
                                } else {
                                    btntitle = item.title;
                                }

                                //if (Global.lang == 7) {
                                //    qbutton +=
                                //        "<button id=" + key + " next='" + item.next + "'value='" + item.next + "' qid='" + pid + "' onclick='tonext(this)' class='choose'>" + btntitle + "</button>";

                                //}
                                //else {
                                //    qbutton +=
                                //        "<button id=" + key + " next='" + item.next + "'value='" + item.next + "' qid='" + pid + "' onclick='tonext(this)'>" + btntitle + "</button>";

                                //}

                                qbutton +=
                                    // "<button id=" + key + " next='" + item.next + "'value='" + item.next + "' qid='" + pid + "' onclick='tonext(this)'>" + btntitle + "</button>";
                                    "<button id=" + key + " next='" + item.next + "'value='" + btntitle + "' qid='" + pid + "' onclick='tonext(this)'>" + btntitle + "</button>";
                            }
                        }
                        if (zsimgurl !== "" && zsimgurl != null) {
                            imgdiv = '<img src="' + zsimgurl + '" />';
                        } else {
                            imgdiv = "";
                        }
                        if (data.data[0].content != null) {
                            miaoshu = data.data[0].content;
                        }
                        infodiv = '<div class="answerbox">' +
                            '<h1>' + wtitle + '</h1>' +
                            '<h6>' + miaoshu + '</h6>' +
                            '<h2>' + imgdiv + '</h2>' +
                            qbutton +
                            '</div>';
                        var option = {
                            div: infodiv,
                            pid: pid,
                            wtitle: wtitle,
                            redio: redio,
                            img: zsimgurl,
                            qbutton: qbutton,
                            tmjson: tmjson,
                            iflast: iflast,
                            tjtype: tjtype
                        };
                        if ($("#mainlayout").length > 0) {
                            $("#mainlayout").unbind().remove();
                        }
                        $.question(option);
                    } else {
                        wtitle = data.data[0].title
                        iflast = 1;
                        toansnow = true;
                        bliuddiv();
                        fastans(pid);
                    }
                } else {
                    iflast = 1;
                    toansnow = true;
                    bliuddiv();
                    fastans(pid);
                }
            }

        }, error: function (data) {

        }
    });
}

function toquestionSearch(t) {
    var pid = t;
    thisqid = pid;
    wtitle = "";
    var redio = "";
    var img = "";
    var qbutton = "";
    var tmjson;
    var btntitle = "";
    var zsimgurl = "";
    var tjtype = "";
    var infodiv = "";
    var imgdiv = "";
    var miaoshu = "";
    $.ajax({
        type: "GET",
        url: "https://kquiz.co/api_contentDetail.php?qId=" + pid + "&source=1",
        // url: "https://api.2loveyou.com:8443/data/center/basic/testQuestions/contentDetail?qId=" + pid + "&source=1",
        crossDomain: true,
        dataType: 'json',
        data: {},
        success: function (data) {
            if (data.data.length == 0) {
                tantxt(get_lan("error"));
            } else {

                updateopen(pid);
                data.data[0].vipId == null ? vipid = "0" : vipid = data.data[0].vipId;
                thisid = data.data[0].id;
                tmjson = JSON.parse(data.data[0].json3);
                fmimg = data.data[0].cover_url3;
                tmtpye = tjtype = data.data[0].type;
                ifImgAnswer = data.data[0].if_img_answer;

                // 2019-10-10 08:34:57 判断是否是趣味题
                if (!(tmtpye == "quwei")) {
                    // 不是 分享封面图
                    share(0);
                    return;
                }

                // 判断是否有选项
                if (data.data[0].if_last != null) {
                    ifHaveQuestion = data.data[0].if_last != 2;
                } else {
                    ifHaveQuestion = true;
                }
                // 判断是否有选项

                $("#app .navbar ul li").removeClass("now");
                if (rebootshare == false) {
                    if (data.data[0].if_last != "2") {
                        toansnow = false;
                        iflast = parseInt((tmjson.length - 1));
                        if (data.data[0].type == "quwei") {
                            if (Global.lang == "0") {
                                wtitle = data.data[0].title + " " + tmjson[iflast].question;
                            } else {
                                wtitle = data.data[0].title;
                            }
                        } else {
                            wtitle = tmjson[iflast].question;
                        }
                        if (tmjson[iflast].audio != null) {
                            if (tmjson[iflast].audio.toString() != "") {
                                redio = "<audio controls='controls' style='width:100%;' src='" + tmjson[x].audio.toString() + "'></audio>";
                            } else {
                                redio = "";
                            }
                        }
                        if (tmjson[iflast].img.toString() != "") {
                            zsimgurl = tmjson[iflast].img + "?x-oss-process=image/resize,w_600";
                        }
                        for (var key in tmjson[iflast].answer) {
                            var item = tmjson[iflast].answer[key];
                            if (item.title != null) {
                                if (item.title.indexOf("img") > 0) {
                                    btntitle = item.title.replace("class='img-responsive'", "class='img' style='border-radius: 10px'");
                                } else {
                                    btntitle = item.title;
                                }
                                //if (Global.lang == 7) {
                                //    qbutton +=
                                //        "<button id=" + key + " next='" + item.next + "'value='" + item.next + "' qid='" + pid + "' onclick='tonext(this)' class='choose'>" + btntitle + "</button>";

                                //}
                                //else {
                                //    qbutton +=
                                //        "<button id=" + key + " next='" + item.next + "'value='" + item.next + "' qid='" + pid + "' onclick='tonext(this)'>" + btntitle + "</button>";

                                //}

                                qbutton +=
                                    "<button id=" + key + " next='" + item.next + "'value='" + item.next + "' qid='" + pid + "' onclick='tonext(this)'>" + btntitle + "</button>";

                            }
                        }
                        if (zsimgurl !== "" && zsimgurl != null) {
                            imgdiv = '<img src="' + zsimgurl + '" />';
                        } else {
                            imgdiv = "";
                        }
                        if (data.data[0].content != null) {
                            miaoshu = data.data[0].content;
                        }
                        infodiv = '<div class="answerbox">' +
                            '<h1>' + wtitle + '</h1>' +
                            '<h6>' + miaoshu + '</h6>' +
                            '<h2>' + imgdiv + '</h2>' +
                            qbutton +
                            '</div>';
                        var option = {
                            div: infodiv,
                            pid: pid,
                            wtitle: wtitle,
                            redio: redio,
                            img: zsimgurl,
                            qbutton: qbutton,
                            tmjson: tmjson,
                            iflast: iflast,
                            tjtype: tjtype
                        };
                        if ($("#mainlayout").length > 0) {
                            $("#mainlayout").unbind().remove();
                        }
                        $.question(option);
                    } else {
                        wtitle = data.data[0].title
                        iflast = 1;
                        toansnow = true;
                        bliuddiv();
                        fastans(pid);
                    }
                } else {
                    iflast = 1;
                    toansnow = true;
                    bliuddiv();
                    fastans(pid);
                }
            }

        }, error: function (data) {

        }
    });
}

function scroll(t) {
    pagenum = 0;
    nScrollHight = 0;
    nScrollTop = 0;
    nDivHight = $("#mainlayout").height();
    $("#mainlayout").scroll(function () {
        Global.isbool = true;
        if ($(".mlayoutlist").length / 10 < 20) {
            nScrollHight = $(this)[0].scrollHeight;
            nScrollTop = $(this)[0].scrollTop;
            if (nScrollTop + nDivHight + 100 >= nScrollHight && Global.isbool == true) {
                Global.isbool = false;
                if (t == 0) {
                    addinfo();
                } else {
                    addtjinfo();
                }
            }
        }
    });
}

function tonext(t) {
    var logged = FBInstant.logEvent(
        'Optionclick',
        1,
    );
    console.log(logged);
    $("#mainlayout .answerbox button").addClass("unclick");
    $("#loading-ansewer").removeClass("hide");
    //if (tmtpye == "quwei" || tmtpye == "ip") {
    //    if ($(".answerbox").length > 0) {
    //        //$(".answerbox").remove();
    //        //$("#loading").removeClass("hide");
    //    }
    //}
    var next = $(t).attr("next");
    var choose = $(t).attr("id");
    var infid = $(t).attr("qid");
    var value = $(t).attr("value");
    var p = /[a-z]/i;
    console.log(infid+", "+iflast+", "+value);
    if (infid != null && typeof (iflast) != "undefined") {
        //if (Global.lang == 7) {
        //    if (value == "E") {
        //        if (tmtpye == "quwei" || tmtpye == "ip") {
        //            var one = infid;
        //            var two = iflast;
        //            var three = value;
        //            var four = next;
        //            toanswer(one, two, three, four);
        //        } else {
        //            var one = infid;
        //            var two = iflast;
        //            var three = choose;
        //            var four = next;
        //            toanswer(one, two, three, four);
        //        }
        //        showRewardedVideo(0);
        //    } else {
        //        var one = infid;
        //        var two = iflast;
        //        var three = value;
        //        var four = next;
        //        toanswer(one, two, three, four);
        //        showRewardedVideo(0);
        //    }
        //}
        //else {
            
        //}


        // if (p.test(value) == true) {
        if (p.test(choose) == true) {
            if (value == "E") {
                if (tmtpye == "quwei" || tmtpye == "ip") {
                    if (ifHaveQuestion) {
                        var option = {
                            one: infid,
                            two: iflast,
                            three: value,
                            four: next
                        };
                        $.xunwen(option);
                    } else {
                        var one = infid;
                        var two = iflast;
                        var three = value;
                        var four = next;
                        toanswer(one, two, three, four);
                    }

                } else {
                    if (firsttime == 0) {
                        var option = {
                            one: infid,
                            two: iflast,
                            three: choose,
                            four: next
                        }
                        $.xunwen(option);
                    } else {
                        var newtime = new Date().getTime();
                        var timex = Math.floor((newtime - firsttime) / 1000);
                        if (timex > 1) {
                            var option = {
                                one: infid,
                                two: iflast,
                                three: choose,
                                four: next
                            }
                            $.xunwen(option);
                        } else {
                            var one = infid;
                            var two = iflast;
                            var three = choose;
                            var four = next;
                            toanswer(one, two, three, four);
                        }
                    }
                }

            }
            else {
                if (tmtpye == "quwei" || tmtpye == "ip") {
                    // 判断是不是有选项
                    var newtime = new Date().getTime();
                    var timex = Math.floor((newtime - firsttime) / 1000);
                    if (ifHaveQuestion && timex > 1) {
                        var option = {
                            one: infid,
                            two: iflast,
                            three: value,
                            four: next
                        };
                        $.xunwen(option);
                    } else {
                        var one = infid;
                        var two = iflast;
                        var three = value;
                        var four = next;
                        toanswer(one, two, three, four);
                    }


                } else {
                    if (firsttime == 0) {
                        var option = {
                            one: infid,
                            two: iflast,
                            three: value,
                            four: next
                        };
                        $.xunwen(option);
                    } else {
                        var newtime = new Date().getTime();
                        var timex = Math.floor((newtime - firsttime) / 1000);
                        if (timex > 1) {
                            var option = {
                                one: infid,
                                two: iflast,
                                three: value,
                                four: next
                            }
                            $.xunwen(option);
                        } else {
                            var one = infid;
                            var two = iflast;
                            var three = value;
                            var four = next;
                            toanswer(one, two, three, four);
                        }
                    }
                }
            }
        }



    }
}

function toanswer(a, b, c, d) {

    console.log("toanswer");

    var id = a;
    var cs3 = "&choose=" + c;
    var cs2 = "&index=" + b;
    var anstitle = "";
    var anscontent = "";
    var drawParams;
    bliuddiv();

    if (ifImgAnswer == "1") {
        $.ajax({
            type: "GET",
            // url: "https://api.2loveyou.com:8443/data/center/basic/testImgAnswers/createAnswerImg?qId=" + id + cs3,
            url: "https://kquiz.co/api_contentDetail.php?qId=" + id + cs3,
            data: {},
            dataType: "json", 
            success: function (data) {
                var backurl = data.data.backGroundUrl2 == null ? fmimg : data.data.backGroundUrl2;
                drawParams = {
                    eId: 'finalimg',
                    backImgUrl: backurl,
                    nameText: Global.playername,
                    imgurl: Global.myphoto,
                    nameX: data.data.nameX,
                    nameY: data.data.nameY,
                    nameW: data.data.nameW,
                    nameH: data.data.nameH,
                    headX: data.data.headX,
                    headY: data.data.headY,
                    headW: data.data.headW,
                    headH: data.data.headH,
                    qrCodeX: data.data.qrCodeX,
                    qrCodeY: data.data.qrCodeY,
                    qrCodeW: data.data.qrCodeW,
                    qrCodeH: data.data.qrCodeH,
                    nameColor: data.data.nameColor,
                    headCircle: data.data.headCircle,
                    nameArray: data.data.nameArray
                };
                drawPic(drawParams, anstitle, anscontent);
            }, error: function (reponse) {

            }
        });
    } else if (ifImgAnswer == "2") {

        // 人脸识别题目
        $.ajax({
            type: "POST",
            url: "https://api.2loveyou.com:8443/faceSearch",
            data: {
                groupId: 1,
                passRate: 15,
                headUrl: Global.myphoto,
                ifRand: 1,
                gender: c
            },
            success: function (data) {
                // 画上图片
                var faceName = data['data']['userFaceInfo']['name'];
                var faceSimilarValue = data['data']['similarValue'];
                answerinfo(data['data']['image'], "你的長相和 " + faceName + "  最相似", "相似度:" + faceSimilarValue + "%");
            }, error: function (reponse) {
                console.log("接口错误:", reponse);
            }
        });

    } else {

        $.ajax({
            type: "GET",
            // url: "https://api.2loveyou.com:8443/data/center/basic/testQuestions/detail?qId=" + id + cs2 + cs3,
            url: "https://kquiz.co/api_answer.php?qId=" + id + cs2 + cs3,
            dataType: "json",
            data: {},
            success: function (data) {
                zsimgurl = data.data.pic2 == null ? fmimg : data.data.pic2;
                anstitle = data.data.title1.toString() + data.data.title2.toString();
                anscontent = data.data.content.toString();
                drawParams = {
                    eId: 'finalimg',
                    backImgUrl: zsimgurl,
                    nameText: Global.playername,
                    imgurl: Global.myphoto,
                    nameX: data.data.nameX,
                    nameY: data.data.nameY,
                    nameW: data.data.nameW,
                    nameH: data.data.nameH,
                    headX: data.data.headX,
                    headY: data.data.headY,
                    headW: data.data.headW,
                    headH: data.data.headH,
                    qrCodeX: data.data.qrCodeX,
                    qrCodeY: data.data.qrCodeY,
                    qrCodeW: data.data.qrCodeW,
                    qrCodeH: data.data.qrCodeH,
                    nameColor: data.data.nameColor,
                    headCircle: data.data.headCircle,
                    nameArray: data.data.nameArray
                };
                drawPic(drawParams, anstitle, anscontent);
            }, error: function (reponse) {

            }
        });
    }
}

function fastans(t) {
    //$("#fx").removeClass("hide");
    //$(".listani").css("height", "1.2rem");
    //$("#loading").removeClass("hide");
    var anstitle = "";
    var anscontent = "";
    var drawParams;
    $.ajax({
        type: "GET",
        url: "https://api.2loveyou.com:8443/data/center/basic/answer/findOneRandomByQId?qId=" + t,
        data: {},
        success: function (data) {
            if (ifImgAnswer == "1") {
                drawParams = {
                    eId: 'finalimg',
                    backImgUrl: data.data.backGroundUrl2,
                    nameText: Global.playername,
                    imgurl: Global.myphoto,
                    nameX: data.data.nameX,
                    nameY: data.data.nameY,
                    nameW: data.data.nameW,
                    nameH: data.data.nameH,
                    headX: data.data.headX,
                    headY: data.data.headY,
                    headW: data.data.headW,
                    headH: data.data.headH,
                    qrCodeX: data.data.qrCodeX,
                    qrCodeY: data.data.qrCodeY,
                    qrCodeW: data.data.qrCodeW,
                    qrCodeH: data.data.qrCodeH,
                    nameColor: data.data.nameColor,
                    headCircle: data.data.headCircle,
                    nameArray: data.data.nameArray
                };
                drawPic(drawParams, anstitle, anscontent);
            } else {
                zsimgurl = data.data.pic2;
                anstitle = data.data.title1.toString() + data.data.title2.toString();
                anscontent = data.data.content.toString();
                drawParams = {
                    eId: 'finalimg',
                    backImgUrl: zsimgurl,
                    nameText: Global.playername,
                    imgurl: Global.myphoto,
                    nameX: data.data.nameX,
                    nameY: data.data.nameY,
                    nameW: data.data.nameW,
                    nameH: data.data.nameH,
                    headX: data.data.headX,
                    headY: data.data.headY,
                    headW: data.data.headW,
                    headH: data.data.headH,
                    qrCodeX: data.data.qrCodeX,
                    qrCodeY: data.data.qrCodeY,
                    qrCodeW: data.data.qrCodeW,
                    qrCodeH: data.data.qrCodeH,
                    nameColor: data.data.nameColor,
                    headCircle: data.data.headCircle,
                    nameArray: data.data.nameArray
                };
                drawPic(drawParams, anstitle, anscontent);
            }
        }, error: function (reponse) {

        }
    });
}

function drawPic(params, title, content) {
    Global.gametime = Global.gametime + 1;
    tentm();
    $('#mainlayout').animate({ scrollTop: 0 }, 100);
    var aa = document.createElement("canvas");
    var img = new Image;
    img.setAttribute("crossOrigin", 'Anonymous');
    console.log(params);
    console.log(title);
    console.log(content);
    img.src = params.backImgUrl + '?vs=' + new Date().toJSON();
    img.onload = function () {
        aa.setAttribute("width", img.width + "");
        aa.setAttribute("height", img.height + "");
        var img2 = new Image();
        img2.setAttribute("crossOrigin", 'Anonymous');
        img2.src = params.imgurl;
        var bb = aa.getContext('2d');
        bb.fillStyle = '#99f';
        bb.fillRect(0, 0, 100, 100);
        bb.drawImage(img, 0, 0);

        img2.onerror = function () {
            console.log("get head error");
            if (params.qrCodeX != null) {
                var img3 = new Image();
                img3.setAttribute("crossOrigin", 'Anonymous');
                if (lang == "6") {
                    img3.src = 'https://jssss.oss-us-west-1.aliyuncs.com/img/qrCode6.jpg' + '?vs=' + new Date().toJSON();
                } else {
                    img3.src = 'https://jssss.oss-us-west-1.aliyuncs.com/img/qrCode1.png' + '?vs=' + new Date().toJSON();
                }
                img3.onload = function () {
                    bb.drawImage(img3, params.qrCodeX, params.qrCodeY, params.qrCodeW, params.qrCodeH);
                    if (params.nameArray != null && params.nameArray != '[]') {
                        var nameArrayData = JSON.parse(params.nameArray);
                        for (var i = 0; i < nameArrayData.length; i++) {
                            var name = nameArrayData[i];
                            drawTextAlign(bb, params.nameText, name.x, name.y, name.w, name.h, name.color, name.font, name.size, name.bold)
                        }
                    } else {
                        drawTextAlign(bb, params.nameText, params.nameX, params.nameY, params.nameW, params.nameH, params.nameColor);
                    }
                    drawTextAlign(bb, params.nameText, img.width - 40, img.height - 20, 20, 20, params.nameColor);
                    bb.strokeStyle = "#fff";
                    bb.stroke();
                    setTimeout(function () {
                        var data64 = aa.toDataURL('image/jpeg', 0.5);  //1表示质量(无损压缩)
                        // 展示图片
                        answerinfo(data64, title, content);
                    }, 1000);
                }
            } else {
                if (params.nameArray != null && params.nameArray != '[]') {

                    var nameArrayData = JSON.parse(params.nameArray);
                    for (var i = 0; i < nameArrayData.length; i++) {
                        var name = nameArrayData[i];
                        drawTextAlign(bb, params.nameText, name.x, name.y, name.w, name.h, name.color, name.font, name.size, name.bold)
                    }
                } else {
                    drawTextAlign(bb, params.nameText, params.nameX, params.nameY, params.nameW, params.nameH, params.nameColor);
                }
                drawTextAlign(bb, params.nameText, img.width - 40, img.height - 20, 20, 20, params.nameColor);
                bb.strokeStyle = "#fff";
                bb.stroke();
                setTimeout(function () {
                    var data64 = aa.toDataURL('image/jpeg', 0.5);  //1表示质量(无损压缩)
                    // 展示图片
                    answerinfo(data64, title, content);
                }, 1000);
            }
        }

        img2.onload = function () {
            if (params.headCircle == null || params.headCircle != 1) {
                bb.drawImage(img2, params.headX, params.headY, params.headW, params.headH);
            } else {
                circleImg(bb, img2, params.headX, params.headY, params.headW / 2);
            }
            if (params.qrCodeX != null) {
                var img3 = new Image();
                img3.setAttribute("crossOrigin", 'Anonymous');
                if (lang == "6") {
                    img3.src = 'https://jssss.oss-us-west-1.aliyuncs.com/img/qrCode6.jpg' + '?vs=' + new Date().toJSON();
                } else {
                    img3.src = 'https://jssss.oss-us-west-1.aliyuncs.com/img/qrCode1.png' + '?vs=' + new Date().toJSON();
                }
                img3.onload = function () {
                    bb.drawImage(img3, params.qrCodeX, params.qrCodeY, params.qrCodeW, params.qrCodeH);
                    if (params.nameArray != null && params.nameArray != '[]') {
                        var nameArrayData = JSON.parse(params.nameArray);
                        for (var i = 0; i < nameArrayData.length; i++) {
                            var name = nameArrayData[i];
                            drawTextAlign(bb, params.nameText, name.x, name.y, name.w, name.h, name.color, name.font, name.size, name.bold)
                        }
                    } else {
                        drawTextAlign(bb, params.nameText, params.nameX, params.nameY, params.nameW, params.nameH, params.nameColor);
                    }
                    drawTextAlign(bb, params.nameText, img.width - 40, img.height - 20, 20, 20, params.nameColor);
                    bb.strokeStyle = "#fff";
                    bb.stroke();
                    setTimeout(function () {
                        var data64 = aa.toDataURL('image/jpeg', 0.5);  //1表示质量(无损压缩)
                        // 展示图片
                        answerinfo(data64, title, content);
                    }, 1000);
                }
            } else {
                if (params.nameArray != null && params.nameArray != '[]') {

                    var nameArrayData = JSON.parse(params.nameArray);
                    for (var i = 0; i < nameArrayData.length; i++) {
                        var name = nameArrayData[i];
                        drawTextAlign(bb, params.nameText, name.x, name.y, name.w, name.h, name.color, name.font, name.size, name.bold)
                    }
                } else {
                    drawTextAlign(bb, params.nameText, params.nameX, params.nameY, params.nameW, params.nameH, params.nameColor);
                }
                drawTextAlign(bb, params.nameText, img.width - 40, img.height - 20, 20, 20, params.nameColor);
                bb.strokeStyle = "#fff";
                bb.stroke();
                setTimeout(function () {
                    var data64 = aa.toDataURL('image/jpeg', 0.5);  //1表示质量(无损压缩)
                    // 展示图片
                    answerinfo(data64, title, content);
                }, 1000);
            }

        }

    }
}

function bliuddiv() {
    var info = '<div class="answerbox resultbox">' +
        '<h1> ' + wtitle + '</h1>' +
        '<button onclick="share(0)" class="unclick"><span><img src="images/fd.png"></span>' + get_lan("cchy") + '</button>' +
        '<div class="listani-1" id="loading-ansewer">' +
        '<div id="loading-1">' +
        '<div id="loading-center-1">' +
        '<p>' + get_lan("fenxi") + '.</p>' +
        '<div id="loading-center-absolute-1">' +
        '<div class="object"></div>' +
        '<div class="object"></div>' +
        '<div class="object"></div>' +
        '<div class="object"></div>' +
        '<div class="object"></div>' +
        '<div class="object"></div>' +
        '<div class="object"></div>' +
        '<div class="object"></div>' +
        '<div class="object"></div>' +
        '<div class="object"></div>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '<div class="resultbtns clearfix">' +
        '<button onclick="share(0)" class="unclick"><span><img src="images/fd.png"></span>' + get_lan("cchy") + '</button>' +
        '<button class="unclick">' + get_lan("again") + '</button>' +
        '<button onclick="share(1)" class="unclick">' + get_lan("sharefm") + '</button>' +
        '</div>' +
        '</div >';
    if (toansnow == true) {
        var option = {
            div: info,
            pid: thisqid,
            iflast: iflast,
            tjtype: tmtpye
        };
        if ($("#mainlayout").length > 0) {
            $("#mainlayout").unbind().remove();
        }
        $.question(option);
    } else {
        $(".answerbox").remove();
        $("#mainlayout").prepend(info);
    }

}

function answerinfo(img, title, content) {
    var infodiv = '<h2><img onclick="share(0)" src="' + img + '" /></h2>' +
        '<p>' + title + '</p>' +
        '<p>' + content + '</p>' +
        '<div class="zan clearfix">' +
        '<div class="zanlist">' +
        '<img src="images/y.png" />' +
        '<p>' + get_lan("yes") + '</p>' +
        '</div>' +
        '<div class="zanlist">' +
        '<img src="images/n.png" />' +
        '<p>' + get_lan("no") + '</p>' +
        '</div>' +
        '</div>';
    $("#loading-ansewer").addClass("hide");
    //if (Global.lang == "7") {
    //    if (rebootshare != true) {
    //        if (firsttime == 0) {
    //            showInterstitial();
    //        }
    //        else {
    //            var newtime = new Date().getTime();
    //            var timex = Math.floor((newtime - firsttime) / 1000);
    //            if (timex > 30) {
    //                showInterstitial();
    //            }
    //        }
    //    }
    //}
    $("#loading-ansewer").after(infodiv);
    $(".unclick").removeClass("unclick");
}

function dzsc(t, u) {
    var pid = u;
    if (t == 0) {
        var type = "&notAccurateNumber=1";
    } else {
        var type = "&accurateNumber=1";
    }
    $.ajax({
        type: "GET",
        url: "https://api.2loveyou.com:8443/data/center/basic/tq/updateByQId?" + "questionId=" + pid + type,
        data: {},
        success: function (reponse) {
            console.log("updata is save");
        }, error: function (reponse) {

        }
    });
}

function share(t, a, callback) {
    $.shareload();
    var imgLink = "";
    var data;
    if (t == 0) {
        if (tmtpye == "quwei" || tmtpye == "ip") {
            imgLink = $("#app .resultbox h2 img").attr("src");
        } else {
            imgLink = fmimg + '?vs=' + new Date().toJSON();
        }
    } else {
        imgLink = fmimg + '?vs=' + new Date().toJSON();
    }
    var aa = document.createElement("canvas");
    var img = new Image;
    img.setAttribute("crossOrigin", 'Anonymous');
    img.src = imgLink;
    img.onload = function () {
        aa.setAttribute("width", img.width + "");
        aa.setAttribute("height", img.height + "");
        var bb = aa.getContext('2d');
        bb.fillStyle = '#99f';    //   填充颜色
        bb.fillRect(0, 0, 100, 100);
        bb.drawImage(img, 0, 0);
        drawTextAlign(bb, Global.playername, img.width - 40, img.height - 20, 20, 20);
        bb.strokeStyle = "#fff";
        bb.stroke();
        setTimeout(function () {
            var data64 = aa.toDataURL('image/jpeg', 0.5);  //1表示质量(无损压缩)
            // 展示图片
            if (rebootshare == true) {
                if (a != null) {
                    data = {
                        from: "async",
                        shareqid: thisqid,
                        shareid: thisid,
                        authorid: vipid,
                        shareuid: Global.playerid,
                        auto: a + "_" + thisqid
                    };
                }
            } else {
                data = { from: "async", shareqid: thisqid, shareuid: Global.playerid, shareid: thisid, authorid: vipid };
            }
            FBInstant.shareAsync({
                intent: 'SHARE',
                image: data64,
                text: '',
                data: data
            }).then(function () {
                $.shareload();
                rebootshare = false;
                console.log("shareAsync is finished!");
                if (callback != null) {
                    return callback(thisqid);
                }
            }).catch(function (e) {
                $.shareload();
                if (callback != null) {
                    return callback(-1);
                }
            });
        }, 1000);
    }
}

function messenger(a, b, callback) {
    $.ajax({
        type: "GET",
        url: "https://api.2loveyou.com:8443/data/center/basic/testQuestions/canShare/message?language=" + a,
        data: {},
        success: function (data) {
            messqid = data.data.questionId;
            messid = data.data.id;
            $.ajax({
                type: "GET",
                url: "https://kquiz.co/api_contentDetail.php?qId=" + messqid + "&source=1",
                // url: "https://api.2loveyou.com:8443/data/center/basic/testQuestions/contentDetail?qId=" + messqid + "&source=1",
                crossDomain: true,
                dataType: 'json',
                data: {},
                success: function (data) {
                    $.shareload();
                    if (data.data[0].h_cover_url != null && data.data[0].h_cover_url != "") {
                        messfmimg = data.data[0].h_cover_url;
                    } else {
                        messfmimg = data.data[0].cover_url3;
                    }

                    var imgLink = "";
                    var data = {
                        from: "async",
                        shareqid: messqid,
                        shareid: messid,
                        shareuid: Global.playerid,
                        auto: b + "_" + messid
                    };
                    imgLink = messfmimg + '?vs=' + new Date().toJSON();
                    var aa = document.createElement("canvas");
                    var img = new Image;
                    img.setAttribute("crossOrigin", 'Anonymous');
                    img.src = imgLink;
                    img.onload = function () {
                        aa.setAttribute("width", img.width + "");
                        aa.setAttribute("height", img.height + "");
                        var bb = aa.getContext('2d');
                        bb.fillStyle = '#99f';    //   填充颜色
                        bb.fillRect(0, 0, 100, 100);
                        bb.drawImage(img, 0, 0);
                        drawTextAlign(bb, Global.playername, img.width - 40, img.height - 20, 20, 20);
                        bb.strokeStyle = "#fff";
                        bb.stroke();
                        setTimeout(function () {
                            var data64 = aa.toDataURL('image/jpeg', 0.5);  //1表示质量(无损压缩)
                            // 展示图片

                            FBInstant.context.chooseAsync({
                                filters: ['NEW_CONTEXT_ONLY'],
                                minSize: 10,
                            }).then(function () {
                                FBInstant.updateAsync({
                                    action: 'CUSTOM',
                                    cta: 'Join The Game',
                                    image: data64,
                                    text: {
                                        default:
                                            '太準了，妳也來試一下！',
                                        localizations: {
                                            en_US: 'Come on baby，Join The Game!',
                                            zh_CN: '太準了，妳也來試一下！!',
                                            ja_JP: 'Come on baby，Join The Game!'
                                        }
                                    },
                                    template: 'WORD_PLAYED',
                                    data: data,
                                    strategy: 'IMMEDIATE',
                                    notification: 'NO_PUSH',
                                }).then(function () {
                                    $.shareload();
                                    return callback(messqid);
                                })
                            }).catch(function (e) {
                                $.shareload();
                                return callback(-1);
                            })
                        }, 1000);
                    }

                }, error: function (data) {

                }
            });

        }, error: function (data) {

        }
    });
}

function updateopen(t) {
    var infid = t;
    $.ajax({
        type: "GET",
        url: "https://api.2loveyou.com:8443/data/center/basic/tq/updateByQId?questionId=" + infid + "&openCount=1",
        data: {},
        success: function (reponse) {
            console.log("updateopen+1");
        }, error: function (reponse) {

        }
    });
}

function translation(t) {
    if (Global.locale == "ja_JP" || Global.locale == "ja_KS" || Global.locale == "ja_CN") {
        Global.lang = "6";
    } else if (Global.locale == "zh_CN" || Global.locale == "zh_HK" || Global.locale == "zh_TW") {
        Global.lang = "0";
    } else if (Global.locale == "pt_PT" || Global.locale == "pt_BR") {
        Global.lang = "7";
    } else if (Global.locale.indexOf("en") > -1) {
        Global.lang = "1";
    } else if (Global.locale.indexOf("fr") > -1) {
        Global.lang = "8";
    } else if (Global.locale.indexOf("es") > -1) {
        Global.lang = "7";
    } else if (Global.locale.indexOf("de") > -1) {
        Global.lang = "9";
    } else if (Global.locale.indexOf("th") > -1) {
        Global.lang = "10";
    }
    else {
        Global.lang = "1";
    }

    $('[set-lan]').each(function () {
        var me = $(this);
        var a = me.attr('set-lan').split(':');
        var p = a[0];   //文字放置位置
        var m = a[1];   //文字的标识

        //用户选择语言后保存在cookie中，这里读取cookie中的语言版本
        var lan = Global.lang;
        var t;
        //选取语言文字
        switch (lan) {
            case '0':
                t = zn[m];  //这里cn[m]中的cn是上面定义的json字符串的变量名，m是json中的键，用此方式读取到json中的值
                break;
            case '6':
                t = jp[m];
                break;
            case '1':
                t = en[m];
                break;
            case '7':
                t = pt[m];
                break;
            case '8':
                t = fr[m];
                break;
            case "9":
                t = de[m];
                break;
            case "10":
                t = th[m];
                break;
            default:
                t = en[m];
        }

        //如果所选语言的json中没有此内容就选取其他语言显示
        if (t == undefined) t = en[m];
        if (t == undefined) return true;   //如果还是没有就跳出
        //文字放置位置有（html,val等，可以自己添加）
        switch (p) {
            case 'html':
                me.html(t);
                break;
            case 'val':
            case 'value':
                me.val(t);
                break;
            default:
                me.html(t);
        }

    });
    if (t == 0) {
        main();
    } else {
        getinfo(0);
    }
}

function get_lan(m) {
    //获取文字
    var lan = Global.lang;     //语言版本
    var t;
    //选取语言文字
    switch (lan) {
        case '0':
            t = zn[m];
            break;
        case '6':
            t = jp[m];
            break;
        case '1':
            t = en[m];
            break;
        case '7':
            t = pt[m];
            break;
        case "8":
            t = fr[m];
            break;
        case "9":
            t = de[m];
            break;
        case "10":
            t = th[m];
            break;
        default:
            t = zn[m];
    }

    //如果所选语言的json中没有此内容就选取其他语言显示
    if (t == undefined) {
        t = en[m];
    }
    if (t == undefined) {
        t = m;
    } //如果还是没有就返回他的标识
    return t;
}

function reboot(a, b) {
    var type = "";
    if (b == 0) {
        type = "&type=quwei";
    } else {
        type = "&game=whh-quweiNot"
    }
    $.ajax({
        type: "GET",
        url: "https://api.2loveyou.com:8443/data/center/basic/testQuestions/canShare?language=" + a + type,
        data: {},
        success: function (data) {
            var pid = data.data;
            console.log(pid)
            rebootshare = true;
            toquestion(pid);
        }, error: function (reponse) {

        }
    });
}

function vipreboot(a,b) {
    $.ajax({
        type: "GET",
        url: "https://api.2loveyou.com:8443/data/center/basic/testVipShareLogs/findOneToShare?vipId=" + b + "&language="+a,
        data: {},
        success: function (data) {
            var pid = data.data.question_id;
            console.log(pid)
            rebootshare = true;
            toquestion(pid);
        }, error: function (reponse) {

        }
    });
}


function tentm() {
    if (Global.gametime % 10 == 0 && sharer != "") {
        if (sharer.split("_")[0] != Global.playerid) {

            if (Global.lang = "0") {
                $.ajax({
                    type: "POST",
                    url: "https://stat.fbquickgames.com/stat/levelin?fid=" + sharer.split("_")[0] + "&gid=487028831860750&qid=" + sharer.split("_")[1],
                    data: {},
                    success: function (data) {
                        console.log("10 gametimes is save")
                    },
                    error: function (reponse) {
                    }
                })
            }
            else if(Global.lang = "7"){ 
                $.ajax({
                    type: "POST",
                    url: "https://brword.borderlessbd.com/stat/levelin?fid=" + sharer.split("_")[0] + "&gid=487028831860750&qid=" + sharer.split("_")[1],
                    data: {},
                    success: function (data) {
                        console.log("10 gametimes-brword is save")
                    },
                    error: function (reponse) {
                    }
                })
            }
            else if(Global.lang = "1"){ 
                $.ajax({
                    type: "POST",
                    url: "https://zaword.borderlessbd.com/stat/levelin?fid=" + sharer.split("_")[0] + "&gid=487028831860750&qid=" + sharer.split("_")[1],
                    data: {},
                    success: function (data) {
                        console.log("10 gametimes-zaword is save")
                    },
                    error: function (reponse) {
                    }
                })
            }
            else if(Global.lang = "6"){ 
                $.ajax({
                    type: "POST",
                    url: "https://jpword.borderlessbd.com/stat/levelin?fid=" + sharer.split("_")[0] + "&gid=487028831860750&qid=" + sharer.split("_")[1],
                    data: {},
                    success: function (data) {
                        console.log("10 gametimes-jpword is save")
                    },
                    error: function (reponse) {
                    }
                })
            }
            else if(Global.lang = "8"){ 
                $.ajax({
                    type: "POST",
                    url: "https://frword.borderlessbd.com/stat/levelin?fid=" + sharer.split("_")[0] + "&gid=487028831860750&qid=" + sharer.split("_")[1],
                    data: {},
                    success: function (data) {
                        console.log("10 gametimes-frword is save")
                    },
                    error: function (reponse) {
                    }
                })
            }
        }
    }
}

//Base64转码
function getBase64Image(img) {
    var canvas = document.createElement("canvas");
    canvas.width = img.width;
    canvas.height = img.height;
    var ctx = canvas.getContext("2d");
    ctx.drawImage(img, 0, 0, img.width, img.height);
    var dataURL = canvas.toDataURL('image/jpeg', 0.5);
    return dataURL;
}

function drawTextAlign(bb, text, nameX, nameY, nameW, nameH, nameColor, nameFont, nameSize, nameBold) {
    if (nameFont == null || nameFont == "") {
        nameFont = "Adobe Ming Std"; // 默认字体
    }
    if (nameBold == null || nameBold == "") {
        nameBold = 0; // 默认加粗
    }
    bb.fillStyle = nameColor == null ? "#000" : nameColor;   // 文字填充颜色
    bb.font = (nameH * 0.8) + 'px ' + nameFont + (nameBold == 1 ? " bold" : "");
    var textWidth = bb.measureText(text).width;
    var leftBlank = (nameW - textWidth) / 2;
    if (leftBlank < 0) {
        leftBlank = 0;
        bb.font = (nameH * 0.8) * (nameW / textWidth) + 'px ' + nameFont + (nameBold == 1 ? " bold" : "");
    }
    bb.fillText(text, nameX + leftBlank, nameY + (nameH * 0.8));
}

function circleImg(ctx, img, x, y, r) {
    ctx.save();
    var d = 2 * r;
    var cx = x + r;
    var cy = y + r;
    ctx.arc(cx, cy, r, 0, 2 * Math.PI);
    ctx.clip();
    ctx.drawImage(img, x, y, d, d);
    ctx.restore();
}
